import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable, Subject, bindNodeCallback} from "rxjs/index";
import { Router } from '@angular/router';
import { BookData } from './bookList.services';
import { Book } from '../bookList';

@Injectable()
export class ApiService {

  constructor(private http: HttpClient, private router: Router) { }
  baseUrl: string = 'api/bookList';
  headers = new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json');
  httpOptions = {
    headers: this.headers
  };

  getBookList() : Observable<BookData[]> {
    return this.http.get<BookData[]>(this.baseUrl);
  }

  getBookById(id: number): Observable<Book> {
    return this.http.get<Book>(`${this.baseUrl}/${id}`);
  }

  createBookDetails(bookData): Observable<Book> {
    return this.http.post<Book>(this.baseUrl, bookData, this.httpOptions)
  }

  updateBook(bookData): Observable<Book> {
    return this.http.put<Book>(this.baseUrl, bookData, this.httpOptions);
  }

  deleteBook(id: number): Observable<Book> {
    return this.http.delete<Book>(`${this.baseUrl}/${id}`,this.httpOptions);
  }
}