
import {InMemoryDbService} from 'angular-in-memory-web-api';
import {Book} from './../bookList';


export class BookData implements InMemoryDbService {
  createDb(){
    const bookList: Book[]=[
    {
        "id": 1,
        "title": "Angular",
        "price": 3000,
        "author": "Google",
        "ratings": 3,
        "category": "Nature",
        "description": ""
    },
    {
        "id": 2,
        "title": "Javascript Language",
        "price": 2000,
        "author": "Google",
        "ratings": 3,
        "category": "Nature",
        "description": ""
    },
    {
        "id": 3,
        "title": "React js",
        "price": 2000,
        "author": "Facebook",
        "ratings": 3,
        "category": "Fiction category",
        "description": ""
    },
    {
      "id": 4,
      "title": "React Native",
      "price": 2000,
      "author": "Facebook",
      "ratings": 3,
      "category": "Fiction category",
      "description": ""
  },
  {
    "id": 5,
    "title": ".Net",
    "price": 2000,
    "author": "Facebook",
    "ratings": 3,
    "category": "Nature",
    "description": "Language"
},
{
  "id": 6,
  "title": "Java",
  "price": 2000,
  "author": "Facebook",
  "ratings": 3,
  "category": "Nature",
  "description": ""
},{
  "id": 7,
  "title": "C",
  "price": 2000,
  "author": "Facebook",
  "ratings": 3,
  "category": "Nature",
  "description": ""
}
    ];
    return {bookList};
  }
}