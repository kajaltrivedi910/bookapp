import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../services/apiServices';
import { BookData } from '../services/bookList.services';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})

export class BookListComponent implements OnInit {

  bookLists: BookData[]=[];

  sortName: string | null = null;
  sortValue: string | null = null;

  listOfDisplayData = [];
  loading = true;
  pageSize = 5;

  constructor(private router: Router, private apiService: ApiService) { }

  ngOnInit() {
    if(!localStorage.getItem('token')) {
      this.router.navigate(['login']);
      return;
    }
  
    // fetch book details list api and dispaly data in table
    this.apiService.getBookList()
      .subscribe( (data:any) => {
        this.bookLists = data;
        this.loading = false;
        this.listOfDisplayData = [
          ...this.bookLists
        ]
      });
  }

  // delete record using api
  deleteBook(book): void {
    this.apiService.deleteBook(book.id)
      .subscribe( data => {
        this.listOfDisplayData = this.listOfDisplayData.filter(u => u !== book);
      })
  };

  // redirect of edit book details page for edit record
  editBook(book): void {
    this.router.navigate([`bookdetails/edit/${book.id}`]);
  };

   // redirect of add book details page for add new book record
  addBook(): void {
    this.router.navigate(['bookdetails/add']);
  };

    /**
   * SORTING TABLE DATA
   */
  sort(sort: { key: string; value: string }): void {
    this.sortName = sort.key;
    this.sortValue = sort.value;
    this.search();
  }

  search(): void {
    /** sort data **/
    if (this.sortName && this.sortValue) {
      this.listOfDisplayData = this.bookLists.sort((a, b) =>
        this.sortValue === 'ascend'
          ? a[this.sortName!] > b[this.sortName!]
            ? 1
            : -1
          : b[this.sortName!] > a[this.sortName!]
          ? 1
          : -1
      );
    } else {
      this.listOfDisplayData = this.bookLists;
    }
  }

  // Search data by book title
  searchValue = '';
  commanSearch(): void {
    this.listOfDisplayData = this.bookLists.filter((item: any) => item.title.indexOf(this.searchValue) !== -1);
  }

}
