import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../services/apiServices';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.scss']
})
export class BookDetailsComponent implements OnInit {

  page = 'Add';
  id;
  constructor(private formBuilder: FormBuilder,private router: Router, private apiService: ApiService, private activeRoute: ActivatedRoute) {
    this.activeRoute.params.subscribe((param) => {
      if(param.id) {
        this.page = 'Edit'
        this.id = param.id;
      }
    })
   }

  addForm: FormGroup;

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      id: [],
      title: ['', Validators.required],
      price: ['', Validators.required],
      author: ['', Validators.required],
      ratings: [3],
      category: ['', Validators.required],
      description: ['', Validators.required]

    });
    if(this.page === 'Edit') {
      this.fetchBookDetailsById()
    }

  }

  /**
   * Create and edit using opertion perfome method
   * Using angular-in-memory-web-api create BookData database for perfome all opertion
   *  
   */

  // fetch book details data by id using get api
  // patch form value
  fetchBookDetailsById() {
    this.apiService.getBookById(this.id).subscribe( (data:any) => { 
      this.addForm.patchValue(data)
    })
  }

  /**
   * Create book details using post api and edit book details using put api
   *  
   */
  onSubmit() {
    if (this.addForm.invalid) {
      return;
    }
    let fromData = this.addForm.value;
    // Create data
    if(this.page === 'Add') {
      this.apiService.getBookList().subscribe((booklistData:any) => {
          // Increase id 
          let maxIndex = booklistData.length - 1;
          let bookWithMaxIndex = booklistData[maxIndex];
					let bookId = bookWithMaxIndex.id + 1;
          fromData.id = bookId;
          this.apiService.createBookDetails(fromData).subscribe(data => {
            this.router.navigate(['booklist'])
          });
      })
    
    }
    // update data
    else {
      this.apiService.updateBook(this.addForm.value).subscribe(data => {
        this.router.navigate(['booklist'])
      });
    }
  }

}
