import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  userName = ''
  constructor(public router: Router) { }

  ngOnInit(): void {
    this.userName = localStorage.getItem('userName')
  }

  logout() {
    localStorage.removeItem('userName')
    this.router.navigate(['login'])
  }

}
