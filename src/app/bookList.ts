export class Book {
    constructor (public id = 0 , public title = "", public price = 0,
    public  author = "", public ratings = 0, public category = "", public description = "") {}
}